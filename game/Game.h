#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QQmlApplicationEngine>
#include "view/cpp/QmlMapInterface.h"
#include "view/cpp/PlayerIntentionsDeliver.h"
#include "view/cpp/CPPGlobalEventsDeliver.h"

class Game final : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Game)
public:
    // throws std::runtime_exception if could not receive
    // some of needed QML objects from engine
    explicit Game(
            QQmlApplicationEngine & engine,
            QObject * parent = 0);
    ~Game();

private:
    void terminate();

private slots:
    void onRestartRequired();
    void onApplicationTerminating();

private:
    QmlMapInterface * const qmlMapInterface;
    PlayerIntentionsDeliver * const playerIntentionsDeliver;
    CPPGlobalEventsDeliver * const cppGlobalEventsDeliver;
    Map * map;

};

#endif // GAME_H
