#ifndef STATICMAPLAYERLISTENER_H
#define STATICMAPLAYERLISTENER_H

#include "StaticMapLayer.h"
#include "StaticObject.h"

class StaticMapLayerListener
{
public:
    virtual ~StaticMapLayerListener() {}
    virtual void onStaticObjectChanged(const StaticMapLayer::size_type x,
                                       const StaticMapLayer::size_type y,
                                       const StaticObject & object) = 0;
};

#endif // STATICMAPLAYERLISTENER_H
