#include "ShotMovementStrategy.h"

Mover::Direction ShotMovementStrategy::onPositionReached(
        const StaticMapLayer::size_type,
        const StaticMapLayer::size_type)
{
    return Mover::Direction::Y_INCREASE;
}

Mover::Direction ShotMovementStrategy::onMetObstacle(
        const StaticMapLayer::size_type,
        const StaticMapLayer::size_type)
{
    return Mover::Direction::Y_INCREASE;
}

void ShotMovementStrategy::onStart()
{
    if (getMover() != nullptr && getOwner() != nullptr)
    {
        getMover()->move(*getOwner(), Mover::Direction::Y_INCREASE);
    }
}
