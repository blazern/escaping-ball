#include "RulesMaster.h"

#include <vector>
#include <math.h>
#include <algorithm>
#include "Constants.h"
#include "RulesMasterListener.h"
#include "ShotMovementStrategy.h"

RulesMaster::RulesMaster(std::shared_ptr<StaticMapLayer> & staticMapLayer,
                         std::shared_ptr<DynamicMapLayer> & dynamicMapLayer) :
    finished(false),
    staticMapLayer(staticMapLayer),
    dynamicMapLayer(dynamicMapLayer),
    listeners(),
    shotsThread(nullptr)
{
}

RulesMaster::~RulesMaster()
{
    terminate();
}

void RulesMaster::addListener(RulesMasterListener & listener)
{
    listeners.push_back(&listener);
}

void RulesMaster::removeListener(RulesMasterListener & listener)
{
    listeners.erase(std::find(listeners.begin(), listeners.end(), &listener));
}

void RulesMaster::onStart()
{
    if (shotsThread.get() == nullptr)
    {
        shotsThread = std::shared_ptr<std::thread>(new std::thread([this]() {
            std::shared_ptr<StaticMapLayer> threadPtrStaticMapLayer(staticMapLayer);
            std::shared_ptr<DynamicMapLayer> threadPtrDynamicMapLayer(dynamicMapLayer);

            while (!finished)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(timeBetweenShots));

                for (StaticMapLayer::size_type x = 1; x < threadPtrStaticMapLayer->getWidth() - 1; x++)
                {
                    if (rand() % 4 == 0 || x == threadPtrStaticMapLayer->getWidth() - 2)
                    {
                        threadPtrDynamicMapLayer->addDynamicObject(
                                    new DynamicObject(
                                        DynamicObject::Type::SHOT,
                                        std::shared_ptr<DynamicObjectMovementStrategy>(
                                            new ShotMovementStrategy())),
                                    x,
                                    0);
                    }
                }
            }
        }));

        shotsThread->detach();
    }
}

void RulesMaster::terminate()
{
    finished = true;
    timeBetweenShots = 0;
}

RulesMaster::MovementEvent RulesMaster::onObjectTriesToChangePosition(
        DynamicObjectMovement & movement,
        const float newX,
        const float newY)
{
    if (staticMapLayer->isOutOfBounds(newX, newY))
    {
        if (movement.getObject().getType() == DynamicObject::Type::SHOT)
        {
            dynamicMapLayer->removeObject(movement.getPositionedObject());
            return MovementEvent::OBJECT_SUICIDE;
        }
        else
        {
            letMovementRedirectBecauseOfObstacle(movement);
            return MovementEvent::OBSTACLE_MET;
        }
    }

    std::vector<StaticMapLayer::Point> occupiedPoints =
            StaticMapLayer::getPointsOccupiedBy(newX, newY);
    for (const auto & occupiedPoint : occupiedPoints)
    {
        const StaticObject & objectAtPoint =
                staticMapLayer->getObject(occupiedPoint.first, occupiedPoint.second);
        if (objectAtPoint.getType() == StaticObject::Type::OBSTACLE)
        {
            if (movement.getObject().getType() == DynamicObject::Type::SHOT)
            {
                dynamicMapLayer->removeObject(movement.getPositionedObject());
                return MovementEvent::OBJECT_SUICIDE;
            }
            else
            {
                letMovementRedirectBecauseOfObstacle(movement);
                return MovementEvent::OBSTACLE_MET;
            }
        }
    }

    return MovementEvent::OBJECT_MOVE;
}

void RulesMaster::letMovementRedirectBecauseOfObstacle(DynamicObjectMovement & movement)
{
    movement = DynamicObjectMovement(
                movement.getPositionedObject(),
                movement.getObject().onMetObstacle(
                    movement.getX(),
                    movement.getY()));
}

bool RulesMaster::canPlaceObstacleAt(const StaticMapLayer::size_type x, const StaticMapLayer::size_type y) const
{
    for (const auto & dynamicObject : dynamicMapLayer->getObjectsCopy())
    {
        const std::vector<StaticMapLayer::Point> occupiedPoints =
                StaticMapLayer::getPointsOccupiedBy(dynamicObject->getX(), dynamicObject->getY());

        for (const auto & point : occupiedPoints)
        {
            if (point.first == x && point.second == y)
            {
                return false;
            }
        }
    }
    return true;
}

bool RulesMaster::canCreateDynamicObject(
        const DynamicObject::Type type,
        const StaticMapLayer::size_type x,
        const StaticMapLayer::size_type y) const
{
    if (staticMapLayer->getWidth() <= x
        || staticMapLayer->getHeight() <= y)
    {
        return false;
    }

    if (type == DynamicObject::Type::SHOT
        && staticMapLayer->getObject(x, y).getType() == StaticObject::Type::CANNON)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void RulesMaster::onObjectMoved(DynamicObjectMovement & movement)
{
    checkPlayerLife(movement.getPositionedObject());
    checkIfObjectReachedStaticPosition(movement);
}

void RulesMaster::checkPlayerLife(const PositionedDynamicObject & object)
{
    if (object.getType() == DynamicObject::Type::PATROL
        || object.getType() == DynamicObject::Type::SHOT)
    {
        const PositionedDynamicObject & player = dynamicMapLayer->getPlayer();

        const auto occupiedByPatrolPositions =
                StaticMapLayer::getPointsOccupiedBy(object.getX(), object.getY());
        const auto occupiedByPlayerPositions =
                StaticMapLayer::getPointsOccupiedBy(player.getX(), player.getY());

        for (const auto & patrolPoint : occupiedByPatrolPositions)
        {
            for (const auto & playerPoint : occupiedByPlayerPositions)
            {
                if (patrolPoint.first == playerPoint.first
                    && patrolPoint.second == playerPoint.second)
                {
                    finished = true;
                    for (auto & listener : listeners)
                    {
                        listener->onGameFinished(GameFinishReason::DEATH);
                    }
                }
            }
        }
    }
}

void RulesMaster::checkIfObjectReachedStaticPosition(DynamicObjectMovement & movement)
{
    const StaticMapLayer::size_type xClosestIndex = StaticMapLayer::getIndexFor(movement.getX());
    const StaticMapLayer::size_type yClosestIndex = StaticMapLayer::getIndexFor(movement.getY());

    if (fabs(movement.getX() - xClosestIndex) <= Constants::EQUAL_FLOATS_DIFF
        && fabs(movement.getY() - yClosestIndex) <= Constants::EQUAL_FLOATS_DIFF)
    {
        checkPlayerFinish(movement.getObject(), xClosestIndex, yClosestIndex);

        const Mover::Direction newDirection =
                movement.getObject().onPositionReached(xClosestIndex, yClosestIndex);

        if (newDirection != movement.getDirection())
        {
            redirectMovement(movement, newDirection);
        }
    }
}

void RulesMaster::checkPlayerFinish(DynamicObject & object, const StaticMapLayer::size_type x, const StaticMapLayer::size_type y)
{
    if (object.getType() == DynamicObject::Type::PLAYER
        && staticMapLayer->getObject(x, y).getType() == StaticObject::Type::FINISH)
    {
        finished = true;
        for (auto & listener : listeners)
        {
            listener->onGameFinished(GameFinishReason::WIN);
        }
    }
}

void RulesMaster::redirectMovement(DynamicObjectMovement & movement, const Mover::Direction newDirection)
{
    movement = DynamicObjectMovement(movement.getPositionedObject(), newDirection);
}
