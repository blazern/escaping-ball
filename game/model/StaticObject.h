#ifndef STATICOBJECT_H
#define STATICOBJECT_H

class StaticObject final
{
public:
    enum Type { TILE, OBSTACLE, FINISH, CANNON };

    explicit StaticObject(const Type type = Type::TILE);
    StaticObject(const StaticObject & other) = default;
    StaticObject & operator=(const StaticObject & other) = default;

    Type getType() const;

private:
    Type type;
};

#endif // STATICOBJECT_H
