#ifndef RULESMASTERLISTENER_H
#define RULESMASTERLISTENER_H

#include "RulesMaster.h"

class RulesMasterListener
{
public:
    virtual ~RulesMasterListener() {}

    virtual void onGameFinished(const RulesMaster::GameFinishReason finishReason) = 0;
};

#endif // RULESMASTERLISTENER_H
