#include "PositionedDynamicObject.h"

#include <stdexcept>

PositionedDynamicObject::PositionedDynamicObject(
        const shared_ptr<DynamicObject> & dynamicObject,
        const float x,
        const float y) :
    dynamicObject(dynamicObject),
    x(x),
    y(y)
{
    if (dynamicObject.get() == nullptr)
    {
        throw std::invalid_argument("dynamicObject must not be nullptr");
    }
}

DynamicObject & PositionedDynamicObject::getObject()
{
    return *dynamicObject;
}

float PositionedDynamicObject::getX() const
{
    return x;
}

float PositionedDynamicObject::getY() const
{
    return y;
}

DynamicObject::Type PositionedDynamicObject::getType() const
{
    return dynamicObject->getType();
}

void PositionedDynamicObject::setX(const float x)
{
    this->x = x;
}

void PositionedDynamicObject::setY(const float y)
{
    this->y = y;
}
