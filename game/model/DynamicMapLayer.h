#ifndef DYNAMICMAPLAYER_H
#define DYNAMICMAPLAYER_H

#include <vector>
#include <memory>
#include <mutex>
#include <utility>
#include "PositionedDynamicObject.h"
#include "PlayerMovementStrategy.h"
#include "StaticMapLayer.h"

using std::vector;
using std::shared_ptr;
using std::pair;

class RulesMaster;
class DynamicMapLayerListener;

class DynamicMapLayer final
{
    typedef vector<shared_ptr<PositionedDynamicObject>> ObjectsConstainer;
    friend class RulesMaster;

public:
    explicit DynamicMapLayer(const StaticMapLayer & staticLayer);
    DynamicMapLayer(const DynamicMapLayer & other) = delete;
    DynamicMapLayer & operator=(const DynamicMapLayer & other) = delete;

    void addListener(DynamicMapLayerListener & listener);
    void removeListener(DynamicMapLayerListener & listener);

    typedef pair<float, float> Point;

    typedef ObjectsConstainer::size_type size_type;
    size_type getSize() const;

    const ObjectsConstainer getObjectsCopy() const;
    ObjectsConstainer getObjectsCopy();

    PositionedDynamicObject * findPositionedObjectWith(const DynamicObject & dynamicObject);
    const PositionedDynamicObject & getPlayer() const;

    void movePlayerTo(const StaticMapLayer::size_type x, const StaticMapLayer::size_type y);

    void setRulesMaster(std::shared_ptr<RulesMaster> & rulesMaster);

    // for friends
private:
    // makes the instance an owner of the object
    // returns true if was added
    // if object was not added, it will be destroyed
    bool addDynamicObject(DynamicObject * object,
                          const StaticMapLayer::size_type x,
                          const StaticMapLayer::size_type y);

    void removeObject(PositionedDynamicObject & object);

private:
    mutable std::mutex objectsMutex;
    ObjectsConstainer objects;
    shared_ptr<PlayerMovementStrategy> playerMovementStrategy;
    std::shared_ptr<RulesMaster> rulesMaster;

    vector<DynamicMapLayerListener *> listeners;
};

#endif // DYNAMICMAPLAYER_H
