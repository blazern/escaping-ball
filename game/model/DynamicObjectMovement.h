#ifndef DYNAMICOBJECTMOVEMENT_H
#define DYNAMICOBJECTMOVEMENT_H

#include <memory>
#include "PositionedDynamicObject.h"
#include "Mover.h"
#include "DynamicMapLayer.h"

class DynamicObjectMovement final
{
public:
    DynamicObjectMovement(PositionedDynamicObject & object,
                          const Mover::Direction direction = Mover::Direction::NONE);
    DynamicObjectMovement(const DynamicObjectMovement &) = default;
    DynamicObjectMovement & operator=(const DynamicObjectMovement &) = default;

    PositionedDynamicObject & getPositionedObject();
    const PositionedDynamicObject & getPositionedObject() const;

    DynamicObject & getObject();
    const DynamicObject & getObject() const;

    float getX() const;
    float getY() const;

    Mover::Direction getDirection() const;

    const std::shared_ptr<DynamicMapLayer::Point> getNextDesiredPosition(const float distanceOfMovement) const;

private:
    PositionedDynamicObject * object;
    Mover::Direction direction;
};

#endif // DYNAMICOBJECTMOVEMENT_H
