#ifndef DYNAMICOBJECTMOVEMENTSTRATEGY_H
#define DYNAMICOBJECTMOVEMENTSTRATEGY_H

#include "Mover.h"
#include "StaticMapLayer.h"

class DynamicObject;

class DynamicObjectMovementStrategy
{
public:
    DynamicObjectMovementStrategy() :
        mover(nullptr),
        owner(nullptr) {}

    virtual ~DynamicObjectMovementStrategy() {}

    // returns new direction to move to
    virtual Mover::Direction onPositionReached(const StaticMapLayer::size_type x,
                                               const StaticMapLayer::size_type y) = 0;
    // returns new direction to move to
    virtual Mover::Direction onMetObstacle(const StaticMapLayer::size_type currentX,
                                           const StaticMapLayer::size_type currentY) = 0;
    void setMover(Mover & mover) { this->mover = &mover; }
    void setOwner(DynamicObject & dynamicObject) { this->owner = &dynamicObject; }

    virtual void onStart() = 0;

protected:
    Mover * getMover() { return mover; }
    const Mover * getMover() const { return mover; }

    DynamicObject * getOwner() { return owner; }
    const DynamicObject * getOwner() const { return owner; }

private:
    Mover * mover;
    DynamicObject * owner;
};

#endif // DYNAMICOBJECTMOVEMENTSTRATEGY_H
