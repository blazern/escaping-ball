#include "PlayerMovementStrategy.h"

PlayerMovementStrategy::PlayerMovementStrategy() :
    distanation(nullptr)
{
}

Mover::Direction PlayerMovementStrategy::onPositionReached(const StaticMapLayer::size_type, const StaticMapLayer::size_type)
{
    distanationMutex.lock();
    const Mover::Direction direction = getDirectionToDistanation();
    distanationMutex.unlock();

    return direction;
}

Mover::Direction PlayerMovementStrategy::getDirectionToDistanation() const
{
    const Mover * const mover = getMover();
    if (distanation != nullptr && mover != nullptr && getOwner() != nullptr)
    {
        StaticMapLayer::Point currentPosition = mover->getIndexesOf(*getOwner());
        long long distanceByX =
                static_cast<long long>(distanation->first)
                - static_cast<long long>(currentPosition.first);
        long long distanceByY =
                static_cast<long long>(distanation->second)
                - static_cast<long long>(currentPosition.second);

        if (distanceByX == 0 && distanceByY == 0)
        {
            return Mover::Direction::NONE;
        }

        if (abs(distanceByX) > abs(distanceByY))
        {
            if (distanceByX > 0)
            {
                return Mover::Direction::X_INCREASE;
            }
            else
            {
                return Mover::Direction::X_DECREASE;
            }
        }
        else
        {
            if (distanceByY > 0)
            {
                return Mover::Direction::Y_INCREASE;
            }
            else
            {
                return Mover::Direction::Y_DECREASE;
            }
        }
    }
    return Mover::Direction::NONE;
}

Mover::Direction PlayerMovementStrategy::onMetObstacle(const StaticMapLayer::size_type, const StaticMapLayer::size_type)
{
    return Mover::Direction::NONE;
}

void PlayerMovementStrategy::moveTo(const StaticMapLayer::size_type x, const StaticMapLayer::size_type y)
{
    distanationMutex.lock();
    distanation.reset(new StaticMapLayer::Point(x, y));
    moveToDistanation();
    distanationMutex.unlock();
}

void PlayerMovementStrategy::moveToDistanation()
{
    Mover * const mover = getMover();
    if (mover != nullptr && getOwner() != nullptr)
    {
        const Mover::Direction directionToDistanation = getDirectionToDistanation();
        mover->move(*getOwner(), directionToDistanation);
    }
}

