#include "PatrolMovementStrategy.h"

PatrolMovementStrategy::PatrolMovementStrategy(const bool initialDirectionIsXIncrease) :
    currentDirection(initialDirectionIsXIncrease ? Mover::Direction::X_INCREASE : Mover::Direction::X_DECREASE)
{
}

Mover::Direction PatrolMovementStrategy::onPositionReached(
        const StaticMapLayer::size_type,
        const StaticMapLayer::size_type)
{
    return currentDirection;
}

Mover::Direction PatrolMovementStrategy::onMetObstacle(
        const StaticMapLayer::size_type,
        const StaticMapLayer::size_type)
{
    if (currentDirection == Mover::Direction::X_INCREASE)
    {
        currentDirection = Mover::Direction::X_DECREASE;
    }
    else
    {
        currentDirection = Mover::Direction::X_INCREASE;
    }

    return currentDirection;
}

void PatrolMovementStrategy::onStart()
{
    getMover()->move(*getOwner(), currentDirection);
}
