#ifndef PLAYERMOVEMENTSTRATEGY_H
#define PLAYERMOVEMENTSTRATEGY_H

#include <utility>
#include <memory>
#include <mutex>
#include "DynamicObjectMovementStrategy.h"
#include "StaticMapLayer.h"

class PlayerMovementStrategy final : public DynamicObjectMovementStrategy
{
public:
    explicit PlayerMovementStrategy();
    PlayerMovementStrategy(const PlayerMovementStrategy &) = delete;
    PlayerMovementStrategy & operator=(const PlayerMovementStrategy &) = delete;

    virtual Mover::Direction onPositionReached(const StaticMapLayer::size_type x,
                                               const StaticMapLayer::size_type y) final override;
    virtual Mover::Direction onMetObstacle(const StaticMapLayer::size_type currentX,
                                           const StaticMapLayer::size_type currentY) final override;

    virtual void onStart() final override {}

    void moveTo(const StaticMapLayer::size_type x, const StaticMapLayer::size_type y);

private:
    void moveToDistanation();
    Mover::Direction getDirectionToDistanation() const;

private:
    std::shared_ptr<StaticMapLayer::Point> distanation;
    std::mutex distanationMutex;

};

#endif // PLAYERMOVEMENTSTRATEGY_H
