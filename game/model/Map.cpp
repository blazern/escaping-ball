#include "Map.h"
#include "Constants.h"

Map::Map() :
    staticMapLayer(new StaticMapLayer()),
    dynamicMapLayer(new DynamicMapLayer(*staticMapLayer)),
    rulesMaster(new RulesMaster(staticMapLayer, dynamicMapLayer)),
    physics(new Physics(dynamicMapLayer, rulesMaster))
{
    dynamicMapLayer->setRulesMaster(rulesMaster);
}

StaticMapLayer & Map::getStaticLayer()
{
    return *staticMapLayer;
}

const StaticMapLayer & Map::getStaticLayer() const
{
    return *staticMapLayer;
}

DynamicMapLayer & Map::getDynamicLayer()
{
    return *dynamicMapLayer;
}

const DynamicMapLayer & Map::getDynamicLayer() const
{
    return *dynamicMapLayer;
}

Physics & Map::getPhysics()
{
    return *physics;
}

const Physics & Map::getPhysics() const
{
    return *physics;
}

RulesMaster & Map::getRulesMaster()
{
    return *rulesMaster;
}

const RulesMaster & Map::getRulesMaster() const
{
    return *rulesMaster;
}

void Map::movePlayerTo(const StaticMapLayer::size_type x, const StaticMapLayer::size_type y)
{
    dynamicMapLayer->movePlayerTo(x, y);
}

void Map::setStaticObject(
        const StaticMapLayer::size_type x,
        const StaticMapLayer::size_type y,
        const StaticObject & object)
{
    if (object.getType() == StaticObject::Type::OBSTACLE
        && !rulesMaster->canPlaceObstacleAt(x, y))
    {
        return;
    }

    staticMapLayer->setObject(x, y, object);
}
