#include "StaticObject.h"

StaticObject::StaticObject(const Type type) :
    type(type)
{
}

StaticObject::Type StaticObject::getType() const
{
    return type;
}
