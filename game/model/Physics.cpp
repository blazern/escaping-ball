#include "Physics.h"

#include <iostream>
#include <math.h>
#include <algorithm>
#include <memory>
#include "Constants.h"

using std::vector;
using std::thread;
using std::mutex;
using std::shared_ptr;
using std::pair;

Physics::~Physics()
{
    dynamicMapLayer->removeListener(*this);
    rulesMaster->removeListener(*this);
    terminate();
    physicsThread.join();
}

Physics::Physics(
        std::shared_ptr<DynamicMapLayer> & dynamicMapLayer,
        std::shared_ptr<RulesMaster> & rulesMaster) :
    finished(false),
    physicsThread(createThread()),
    futureMovementsMutex(),
    objectsMovementsMutex(),
    dynamicMapLayer(dynamicMapLayer),
    rulesMaster(rulesMaster),
    objectsMovements(),
    futureMovements(),
    listeners()
{
    for (auto & positionedDynamicObject : dynamicMapLayer->getObjectsCopy())
    {
        positionedDynamicObject->getObject().setMover(*this);
        objectsMovements.push_back(
                    std::shared_ptr<DynamicObjectMovement>(
                        new DynamicObjectMovement(*positionedDynamicObject, Mover::Direction::NONE)));
    }

    for (auto & movement : objectsMovements)
    {
        movement->getObject().onStart();
    }

    dynamicMapLayer->addListener(*this);
    rulesMaster->addListener(*this);
    rulesMaster->onStart();
}

std::thread Physics::createThread()
{
    const int timeBetweenTicks = TIME_BETWEEN_TICKS;
    return std::thread([this, timeBetweenTicks]() {
        while (!finished)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(timeBetweenTicks));
            moveObjects();
            applyFutureMovements();
        }
    });
}

void Physics::moveObjects()
{
    for (auto & movement : copyObjectsMovements())
    {
        std::shared_ptr<DynamicMapLayer::Point> newPoint = movement->getNextDesiredPosition(DISTANCE_OF_MOVEMENTS);

        if (newPoint.get() != nullptr)
        {
            tryLetMovementToNewPosition(*movement, *newPoint);
        }
    }
}

std::vector<std::shared_ptr<DynamicObjectMovement>> Physics::copyObjectsMovements()
{
    return std::vector<std::shared_ptr<DynamicObjectMovement>>(objectsMovements);
}

const std::vector<std::shared_ptr<DynamicObjectMovement>> Physics::copyObjectsMovements() const
{
    return std::vector<std::shared_ptr<DynamicObjectMovement>>(objectsMovements);
}

void Physics::tryLetMovementToNewPosition(DynamicObjectMovement & movement, const DynamicMapLayer::Point & position)
{
    const RulesMaster::MovementEvent movementTryResult =
            rulesMaster->onObjectTriesToChangePosition(
                movement,
                position.first,
                position.second);

    if (movementTryResult == RulesMaster::MovementEvent::OBJECT_MOVE)
    {
        letMovementToNewPosition(movement, position);
    }
}

void Physics::letMovementToNewPosition(DynamicObjectMovement & movement, const DynamicMapLayer::Point & position)
{
    PositionedDynamicObject & positionedObject = movement.getPositionedObject();

    positionedObject.setX(position.first);
    positionedObject.setY(position.second);

    rulesMaster->onObjectMoved(movement);

    std::lock_guard<std::mutex> mutexLock(listenersMutex);
    vector<PhysicsListener *> listenersCopy(listeners);
    listenersMutex.unlock();
    for (auto & listener : listenersCopy)
    {
        listener->onDynamicObjectMoved(movement.getPositionedObject());
    }
}

void Physics::applyFutureMovements()
{
    std::lock_guard<std::mutex> futureMutexLock(futureMovementsMutex);

    for (auto & movementToChange : copyObjectsMovements())
    {
        auto appliableFutureIterator =
                std::find_if(futureMovements.begin(),
                             futureMovements.end(),
                             [&movementToChange] (std::shared_ptr<DynamicObjectMovement> & movement) {
            return &(movementToChange->getObject()) == &(movement->getObject());
        });

        if (appliableFutureIterator != futureMovements.end())
        {
            *movementToChange = **appliableFutureIterator;
        }
    }

    futureMovements.clear();
}

void Physics::move(const DynamicObject & object, const Mover::Direction direction)
{
    std::lock_guard<std::mutex> mutexLock(futureMovementsMutex);

    for (auto & futureMovement : futureMovements)
    {
        if (&(futureMovement->getObject()) == &object)
        {
            *futureMovement = DynamicObjectMovement(futureMovement->getPositionedObject(), direction);
            return;
        }
    }

    PositionedDynamicObject * const positionedObject =
            dynamicMapLayer->findPositionedObjectWith(object);

    if (positionedObject != nullptr)
    {
        futureMovements.push_back(
                    std::shared_ptr<DynamicObjectMovement>(
                        new DynamicObjectMovement(*positionedObject, direction)));
    }
}

bool Physics::isMoving(const DynamicObject & object) const
{
    for (const auto & movement : copyObjectsMovements())
    {
        if (&object == &movement->getObject()
            && movement->getDirection() != Mover::Direction::NONE)
        {
            return true;
        }
    }
    return false;
}

StaticMapLayer::Point Physics::getIndexesOf(const DynamicObject & object) const
{
    const PositionedDynamicObject * const foundObject = dynamicMapLayer->findPositionedObjectWith(object);

    if (foundObject != nullptr)
    {
        const StaticMapLayer::size_type xClosestIndex = StaticMapLayer::getIndexFor(foundObject->getX());
        const StaticMapLayer::size_type yClosestIndex = StaticMapLayer::getIndexFor(foundObject->getY());
        return StaticMapLayer::Point(xClosestIndex, yClosestIndex);
    }

    return StaticMapLayer::Point(0, 0);
}

void Physics::onGameFinished(const RulesMaster::GameFinishReason)
{
    finished = true;
}

void Physics::onObjectCreated(PositionedDynamicObject & positionedObject)
{
    std::lock_guard<std::mutex> mutexLock(objectsMovementsMutex);

    objectsMovements.push_back(
                std::shared_ptr<DynamicObjectMovement>(
                    new DynamicObjectMovement(positionedObject)));

    objectsMovementsMutex.unlock();

    positionedObject.getObject().setMover(*this);
    positionedObject.getObject().onStart();
}

void Physics::onObjectDestroyed(PositionedDynamicObject & object)
{
    std::lock_guard<std::mutex> mutexLock(objectsMovementsMutex);

    auto newEndIterator = std::remove_if(
                objectsMovements.begin(),
                objectsMovements.end(),
                [&object] (std::shared_ptr<DynamicObjectMovement> & movement) {
        return &object == &(movement->getPositionedObject());
    });

    objectsMovements.erase(newEndIterator, objectsMovements.end());
}

void Physics::terminate()
{
    finished = true;
}

void Physics::addListener(PhysicsListener & listener)
{
    std::lock_guard<std::mutex> mutexLock(listenersMutex);
    listeners.push_back(&listener);
}
