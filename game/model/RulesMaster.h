#ifndef RULESMASTER_H
#define RULESMASTER_H

#include <vector>
#include <thread>
#include <memory>
#include "StaticMapLayer.h"
#include "DynamicMapLayer.h"
#include "DynamicObjectMovement.h"

class RulesMasterListener;

class RulesMaster final
{
public:
    enum class MovementEvent { OBSTACLE_MET, OBJECT_MOVE, OBJECT_SUICIDE };
    enum GameFinishReason { WIN, DEATH };

    explicit RulesMaster(std::shared_ptr<StaticMapLayer> & staticMapLayer,
                         std::shared_ptr<DynamicMapLayer> & dynamicMapLayer);
    ~RulesMaster();

    void addListener(RulesMasterListener & listener);
    void removeListener(RulesMasterListener & listener);

    void onStart();

    // an instance is not supposed to be used after call of terminate()
    void terminate();

    MovementEvent onObjectTriesToChangePosition(DynamicObjectMovement & movement,
                                                const float newX,
                                                const float newY);
    bool canPlaceObstacleAt(const StaticMapLayer::size_type x, const StaticMapLayer::size_type y) const;

    bool canCreateDynamicObject(const DynamicObject::Type type,
                                const StaticMapLayer::size_type x,
                                const StaticMapLayer::size_type y) const;

    void onObjectMoved(DynamicObjectMovement & object);

private:
    void letMovementRedirectBecauseOfObstacle(DynamicObjectMovement & movement);
    void checkPlayerLife(const PositionedDynamicObject & object);
    void checkIfObjectReachedStaticPosition(DynamicObjectMovement & movement);
    void checkPlayerFinish(DynamicObject & object, const StaticMapLayer::size_type x, const StaticMapLayer::size_type y);
    void redirectMovement(DynamicObjectMovement & movement, const Mover::Direction newDirection);

private:
    bool finished;
    std::shared_ptr<StaticMapLayer> staticMapLayer;
    std::shared_ptr<DynamicMapLayer> dynamicMapLayer;
    std::vector<RulesMasterListener *> listeners;

    int timeBetweenShots = 3000;
    std::shared_ptr<std::thread> shotsThread;
};

#endif // RULESMASTER_H
