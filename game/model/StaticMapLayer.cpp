#include "StaticMapLayer.h"

#include <cstdlib>
#include <math.h>
#include "StaticMapLayerListener.h"
#include "Constants.h"

StaticMapLayer::size_type StaticMapLayer::getIndexFor(const float value)
{
    const size_type ceilVal = ceil(value);
    const size_type floorVal = floor(value);
    return fabs(value - floorVal) < fabs(value - ceilVal) ? floorVal : ceilVal;
}

vector<StaticMapLayer::Point> StaticMapLayer::getPointsOccupiedBy(
        const float x,
        const float y)
{
    std::vector<Point> occupiedPoints;

    const size_type xIndex = getIndexFor(x);
    const size_type yIndex = getIndexFor(y);

    const bool singleOccupiedOnX = fabs(x - xIndex) <= Constants::EQUAL_FLOATS_DIFF;
    const bool singleOccupiedOnY = fabs(y - yIndex) <= Constants::EQUAL_FLOATS_DIFF;

    if (singleOccupiedOnX && singleOccupiedOnY)
    {
        occupiedPoints.push_back(Point(xIndex, yIndex));
    }
    else if (!singleOccupiedOnX && singleOccupiedOnY)
    {
        occupiedPoints.push_back(Point(ceil(x), yIndex));
        occupiedPoints.push_back(Point(floor(x), yIndex));
    }
    else if (singleOccupiedOnX && !singleOccupiedOnY)
    {
        occupiedPoints.push_back(Point(xIndex, ceil(y)));
        occupiedPoints.push_back(Point(xIndex, floor(y)));
    }
    else
    {
        occupiedPoints.push_back(Point(ceil(x), ceil(y)));
        occupiedPoints.push_back(Point(ceil(x), floor(y)));
        occupiedPoints.push_back(Point(floor(x), ceil(y)));
        occupiedPoints.push_back(Point(floor(x), floor(y)));
    }

    return occupiedPoints;
}

StaticMapLayer::StaticMapLayer() :
    objects(),
    listeners()
{
    for (size_type x = 0; x < SIZE; x++)
    {
        objects.push_back(vector<StaticObject>());
        for (size_type y = 0; y < SIZE; y++)
        {
            if (y == 1 && x == SIZE - 2)
            {
                objects[x].push_back(
                            StaticObject(StaticObject::Type::FINISH));
            }
            else if (y == SIZE - 1 || x == 0 || x == SIZE - 1)
            {
                objects[x].push_back(
                            StaticObject(StaticObject::Type::OBSTACLE));
            }
            else if (y == 0)
            {
                if (0 < x && x < SIZE - 1)
                {
                    objects[x].push_back(
                                StaticObject(StaticObject::Type::CANNON));
                }
                else
                {
                    objects[x].push_back(
                                StaticObject(StaticObject::Type::OBSTACLE));
                }
            }
            else
            {
                objects[x].push_back(StaticObject(StaticObject::Type::TILE));
            }
        }
    }
}

void StaticMapLayer::addListener(StaticMapLayerListener & listener)
{
    listeners.push_back(&listener);
}

const StaticObject & StaticMapLayer::getObject(const size_type x, const size_type y) const
{
    return objects[x][y];
}

void StaticMapLayer::setObject(const StaticMapLayer::size_type x,
                               const StaticMapLayer::size_type y,
                               const StaticObject & object)
{
    objects[x][y] = object;
    for (auto & listener : listeners)
    {
        listener->onStaticObjectChanged(x, y, object);
    }
}

StaticMapLayer::size_type StaticMapLayer::getWidth() const
{
    return objects.size();
}

StaticMapLayer::size_type StaticMapLayer::getHeight() const
{
    if (objects.size() > 0)
    {
        return objects[0].size();
    }
    return 0;
}

bool StaticMapLayer::isOutOfBounds(const float x, const float y) const
{
    return x < 0 || getWidth() - 1 < x || y < 0 || getHeight() - 1 < y;
}
