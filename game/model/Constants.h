#ifndef CONSTANTS_H
#define CONSTANTS_H

namespace Constants
{
    static constexpr float EQUAL_FLOATS_DIFF = 0.05;
}

#endif // CONSTANTS_H
