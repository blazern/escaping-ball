#ifndef PATROLMOVEMENTSTRATEGY_H
#define PATROLMOVEMENTSTRATEGY_H

#include "DynamicObjectMovementStrategy.h"

class PatrolMovementStrategy final : public DynamicObjectMovementStrategy
{
public:
    explicit PatrolMovementStrategy(const bool initialDirectionIsXIncrease);
    PatrolMovementStrategy(const PatrolMovementStrategy &) = delete;
    PatrolMovementStrategy & operator=(const PatrolMovementStrategy &) = delete;

    virtual Mover::Direction onPositionReached(const StaticMapLayer::size_type x,
                                               const StaticMapLayer::size_type y) final override;
    virtual Mover::Direction onMetObstacle(const StaticMapLayer::size_type currentX,
                                           const StaticMapLayer::size_type currentY) final override;

    virtual void onStart() final override;

private:
    Mover::Direction currentDirection;
};

#endif // PATROLMOVEMENTSTRATEGY_H
