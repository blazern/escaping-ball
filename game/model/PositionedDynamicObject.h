#ifndef POSITIONEDDYNAMICOBJECT_H
#define POSITIONEDDYNAMICOBJECT_H

#include <memory>
#include "DynamicObject.h"

using std::shared_ptr;

class PositionedDynamicObject final
{
public:
    // throws std::invalid_argument if dynamicObject is nullptr
    explicit PositionedDynamicObject(const shared_ptr<DynamicObject> & dynamicObject,
            const float x,
            const float y);
    PositionedDynamicObject(const PositionedDynamicObject & other) = delete;
    PositionedDynamicObject & operator=(const PositionedDynamicObject & other) = delete;

    DynamicObject & getObject();

    float getX() const;
    float getY() const;
    DynamicObject::Type getType() const;

    void setX(const float x);
    void setY(const float y);

private:
    const shared_ptr<DynamicObject> dynamicObject;
    float x;
    float y;
};

#endif // POSITIONEDDYNAMICOBJECT_H
