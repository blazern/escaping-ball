#include "DynamicObjectMovement.h"

DynamicObjectMovement::DynamicObjectMovement(
        PositionedDynamicObject & object,
        const Mover::Direction direction) :
    object(&object),
    direction(direction)
{
}

PositionedDynamicObject & DynamicObjectMovement::getPositionedObject()
{
    return *object;
}

const PositionedDynamicObject & DynamicObjectMovement::getPositionedObject() const
{
    return *object;
}

DynamicObject & DynamicObjectMovement::getObject()
{
    return object->getObject();
}

const DynamicObject & DynamicObjectMovement::getObject() const
{
    return object->getObject();
}

float DynamicObjectMovement::getX() const
{
    return object->getX();
}

float DynamicObjectMovement::getY() const
{
    return object->getY();
}

Mover::Direction DynamicObjectMovement::getDirection() const
{
    return direction;
}

const std::shared_ptr<DynamicMapLayer::Point> DynamicObjectMovement::getNextDesiredPosition(const float distanceOfMovement) const
{
    std::shared_ptr<DynamicMapLayer::Point> newPosition(nullptr);

    switch (getDirection())
    {
    case Mover::Direction::Y_DECREASE:
        newPosition.reset(new DynamicMapLayer::Point(
                              getX(),
                              getY() - distanceOfMovement));
        break;
    case Mover::Direction::X_INCREASE:
        newPosition.reset(new DynamicMapLayer::Point(
                              getX() + distanceOfMovement,
                              getY()));
        break;
    case Mover::Direction::Y_INCREASE:
        newPosition.reset(new DynamicMapLayer::Point(
                              getX(),
                              getY() + distanceOfMovement));
        break;
    case Mover::Direction::X_DECREASE:
        newPosition.reset(new DynamicMapLayer::Point(
                              getX() - distanceOfMovement,
                              getY()));
        break;
    default:
        break;
    }

    return newPosition;
}
