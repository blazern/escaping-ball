#ifndef DYNAMICOBJECT_H
#define DYNAMICOBJECT_H

#include <memory>
#include "Mover.h"
#include "StaticMapLayer.h"
#include "DynamicObjectMovementStrategy.h"

class DynamicObject final
{
public:
    enum Type { PLAYER, PATROL, SHOT };

    explicit DynamicObject(const Type type, const std::shared_ptr<DynamicObjectMovementStrategy> & movementStrategy);
    DynamicObject(const DynamicObject &) = delete;
    DynamicObject & operator=(const DynamicObject &) = delete;

    Type getType() const;

    // returns new direction to move to
    virtual Mover::Direction onPositionReached(const StaticMapLayer::size_type x,
                                               const StaticMapLayer::size_type y);
    // returns new direction to move to
    virtual Mover::Direction onMetObstacle(const StaticMapLayer::size_type currentX,
                                           const StaticMapLayer::size_type currentY);
    void setMover(Mover & mover);

    void onStart();

private:
    const Type type;
    const std::shared_ptr<DynamicObjectMovementStrategy> movementStrategy;
    Mover * mover;
};

#endif // DYNAMICOBJECT_H
