#ifndef PHYSICS_H
#define PHYSICS_H

#include <vector>
#include <utility>
#include <thread>
#include <mutex>
#include <utility>
#include <memory>
#include "Mover.h"
#include "DynamicMapLayer.h"
#include "StaticMapLayer.h"
#include "DynamicObjectMovement.h"
#include "PhysicsListener.h"
#include "RulesMaster.h"
#include "RulesMasterListener.h"
#include "DynamicMapLayerListener.h"

using std::vector;
using std::thread;
using std::mutex;
using std::shared_ptr;
using std::pair;

class Physics final : public Mover, public RulesMasterListener, public DynamicMapLayerListener
{
public:
    ~Physics();
    explicit Physics(shared_ptr<DynamicMapLayer> & dynamicMapLayer,
                     shared_ptr<RulesMaster> & rulesMaster);
    Physics(const Physics & other) = delete;
    Physics & operator=(const Physics & other) = delete;

    void addListener(PhysicsListener & listener);

    virtual void move(const DynamicObject & object, const Direction direction) final override;
    virtual bool isMoving(const DynamicObject & object) const final override;
    virtual StaticMapLayer::Point getIndexesOf(const DynamicObject & object) const final override;

    virtual void onGameFinished(const RulesMaster::GameFinishReason finishReason) final override;
    virtual void onObjectCreated(PositionedDynamicObject & object) final override;
    virtual void onObjectDestroyed(PositionedDynamicObject & object) final override;

    // an instance is not supposed to be used after a call of terminate()
    void terminate();

private:
    thread createThread();
    void moveObjects();
    void tryLetMovementToNewPosition(DynamicObjectMovement & movement, const DynamicMapLayer::Point & position);
    void letMovementToNewPosition(DynamicObjectMovement & movement, const DynamicMapLayer::Point & position);
    void applyFutureMovements();
    vector<shared_ptr<DynamicObjectMovement>> copyObjectsMovements();
    const vector<shared_ptr<DynamicObjectMovement>> copyObjectsMovements() const;

private:
    static const int TIME_BETWEEN_TICKS = 25;
    static constexpr float OBJECTS_SPEED = 2; // tiles per second
    static constexpr float DISTANCE_OF_MOVEMENTS = (TIME_BETWEEN_TICKS / 1000.0) * OBJECTS_SPEED;

private:
    bool finished;
    thread physicsThread;
    mutable mutex futureMovementsMutex;
    mutable mutex objectsMovementsMutex;
    mutable mutex listenersMutex;

    shared_ptr<DynamicMapLayer> dynamicMapLayer;
    shared_ptr<RulesMaster> rulesMaster;

    vector<shared_ptr<DynamicObjectMovement>> objectsMovements;
    vector<shared_ptr<DynamicObjectMovement>> futureMovements;

    vector<PhysicsListener *> listeners;
};

#endif // PHYSICS_H
