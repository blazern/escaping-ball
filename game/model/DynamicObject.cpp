#include "DynamicObject.h"


DynamicObject::DynamicObject(const DynamicObject::Type type,
        const std::shared_ptr<DynamicObjectMovementStrategy> & movementStrategy) :
    type(type),
    movementStrategy(movementStrategy),
    mover(nullptr)
{
    movementStrategy->setOwner(*this);
}

DynamicObject::Type DynamicObject::getType() const
{
    return type;
}

Mover::Direction DynamicObject::onPositionReached(const StaticMapLayer::size_type x,
                                                  const StaticMapLayer::size_type y)
{
    if (movementStrategy.get() != nullptr)
    {
        return movementStrategy->onPositionReached(x, y);
    }
    return Mover::Direction::NONE;
}

Mover::Direction DynamicObject::onMetObstacle(const StaticMapLayer::size_type currentX,
                                              const StaticMapLayer::size_type currentY)
{
    if (movementStrategy.get() != nullptr)
    {
        return movementStrategy->onMetObstacle(currentX, currentY);
    }
    return Mover::Direction::NONE;
}

void DynamicObject::setMover(Mover & mover)
{
    this->mover = &mover;
    if (movementStrategy.get() != nullptr)
    {
        movementStrategy->setMover(mover);
    }
}

void DynamicObject::onStart()
{
    if (movementStrategy.get() != nullptr)
    {
        movementStrategy->onStart();
    }
}
