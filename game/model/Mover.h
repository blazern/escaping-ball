#ifndef MOVER_H
#define MOVER_H

#include "StaticMapLayer.h"

class DynamicObject;

class Mover
{
public:
    virtual ~Mover() {}

    enum Direction { NONE, Y_DECREASE, X_INCREASE, Y_INCREASE, X_DECREASE };

    // required direction will be set somewhen in the future
    virtual void move(const DynamicObject & object, const Direction direction) = 0;
    virtual bool isMoving(const DynamicObject & object) const = 0;
    virtual StaticMapLayer::Point getIndexesOf(const DynamicObject & object) const = 0;
};

#endif // MOVER_H
