#include "DynamicMapLayer.h"

#include <algorithm>
#include "PatrolMovementStrategy.h"
#include "RulesMaster.h"
#include "DynamicMapLayerListener.h"

DynamicMapLayer::DynamicMapLayer(const StaticMapLayer & staticLayer) :
    objectsMutex(),
    objects(),
    playerMovementStrategy(new PlayerMovementStrategy()),
    rulesMaster(nullptr),
    listeners()
{
    objects.push_back(
                std::shared_ptr<PositionedDynamicObject>(
                    new PositionedDynamicObject(
                        std::shared_ptr<DynamicObject>(
                            new DynamicObject(DynamicObject::Type::PLAYER, playerMovementStrategy)),
                        1,
                        staticLayer.getHeight() - 2)));

    for (StaticMapLayer::size_type x = 1; x < staticLayer.getWidth() - 2; x++)
    {
        objects.push_back(
                    std::shared_ptr<PositionedDynamicObject>(
                        new PositionedDynamicObject(
                            std::shared_ptr<DynamicObject>(
                                new DynamicObject(
                                    DynamicObject::Type::PATROL,
                                    std::shared_ptr<PatrolMovementStrategy>(new PatrolMovementStrategy(x % 2 == 1)))),
                            (rand() % (staticLayer.getHeight() - 2)) + 1,
                            x)));
    }
}

void DynamicMapLayer::addListener(DynamicMapLayerListener & listener)
{
    listeners.push_back(&listener);
}

void DynamicMapLayer::removeListener(DynamicMapLayerListener & listener)
{
    listeners.erase(std::find(listeners.begin(), listeners.end(), &listener));
}

DynamicMapLayer::size_type DynamicMapLayer::getSize() const
{
    return objects.size();
}

const DynamicMapLayer::ObjectsConstainer DynamicMapLayer::getObjectsCopy() const
{
    std::lock_guard<std::mutex> mutexLock(objectsMutex);
    return ObjectsConstainer(objects);
}

DynamicMapLayer::ObjectsConstainer DynamicMapLayer::getObjectsCopy()
{
    std::lock_guard<std::mutex> mutexLock(objectsMutex);
    return ObjectsConstainer(objects);
}

PositionedDynamicObject * DynamicMapLayer::findPositionedObjectWith(const DynamicObject & dynamicObject)
{
    std::lock_guard<std::mutex> mutexLock(objectsMutex);

    auto iterator =
            std::find_if(objects.begin(),
                         objects.end(),
                         [&dynamicObject] (std::shared_ptr<PositionedDynamicObject> & positionedObject) {
        return &dynamicObject == &(positionedObject->getObject());
    });

    if (iterator != objects.end())
    {
        return &**iterator;
    }

    return nullptr;
}

const PositionedDynamicObject & DynamicMapLayer::getPlayer() const
{
    return *objects[0];
}

void DynamicMapLayer::movePlayerTo(const StaticMapLayer::size_type x, const StaticMapLayer::size_type y)
{
    playerMovementStrategy->moveTo(x, y);
}

void DynamicMapLayer::setRulesMaster(std::shared_ptr<RulesMaster> & rulesMaster)
{
    this->rulesMaster = rulesMaster;
}

bool DynamicMapLayer::addDynamicObject(
        DynamicObject * object,
        const StaticMapLayer::size_type x,
        const StaticMapLayer::size_type y)
{
    if (object != nullptr
            && rulesMaster != nullptr
            && rulesMaster->canCreateDynamicObject(object->getType(), x, y))
    {
        std::lock_guard<std::mutex> mutexLock(objectsMutex);

        objects.push_back(std::shared_ptr<PositionedDynamicObject>(
                              new PositionedDynamicObject(
                                  std::shared_ptr<DynamicObject>(object),
                                  static_cast<float>(x),
                                  static_cast<float>(y))));
        objectsMutex.unlock();

        for (auto & listener : listeners)
        {
            listener->onObjectCreated(*objects[objects.size() - 1]);
        }

        return true;
    }
    else
    {
        delete object;
        return false;
    }
}

void DynamicMapLayer::removeObject(PositionedDynamicObject & object)
{
    std::lock_guard<std::mutex> firstMutexLock(objectsMutex);

    auto newEndIterator =
            std::remove_if(objects.begin(),
                           objects.end(),
                           [&object] (std::shared_ptr<PositionedDynamicObject> & positionedObject) {
            return &object == positionedObject.get();
    });

    objectsMutex.unlock();

    if (newEndIterator != objects.end())
    {
        for (auto & listener : listeners)
        {
            listener->onObjectDestroyed(object);
        }
        std::lock_guard<std::mutex> secondMutexLock(objectsMutex);
        objects.erase(newEndIterator, objects.end());
    }
}
