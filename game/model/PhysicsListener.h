#ifndef PHYSICSLISTENER_H
#define PHYSICSLISTENER_H

#include "PositionedDynamicObject.h"

class PhysicsListener
{
public:
    virtual ~PhysicsListener() {}
    virtual void onDynamicObjectMoved(const PositionedDynamicObject & object) = 0;
};

#endif // PHYSICSLISTENER_H
