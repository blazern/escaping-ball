#ifndef DYNAMICMAPLAYERLISTENER_H
#define DYNAMICMAPLAYERLISTENER_H

#include "PositionedDynamicObject.h"

class DynamicMapLayerListener
{
public:
    virtual ~DynamicMapLayerListener() {}

    virtual void onObjectCreated(PositionedDynamicObject & object) = 0;

    // is called right before the destruction
    virtual void onObjectDestroyed(PositionedDynamicObject & object) = 0;
};

#endif // DYNAMICMAPLAYERLISTENER_H
