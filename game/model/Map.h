#ifndef MAP_H
#define MAP_H

#include <memory>
#include "StaticMapLayer.h"
#include "DynamicMapLayer.h"
#include "Physics.h"
#include "RulesMaster.h"

class Map final
{
public:
    explicit Map();
    Map(const Map & other) = delete;
    Map & operator=(const Map & other) = delete;

    StaticMapLayer & getStaticLayer();
    const StaticMapLayer & getStaticLayer() const;

    DynamicMapLayer & getDynamicLayer();
    const DynamicMapLayer & getDynamicLayer() const;

    Physics & getPhysics();
    const Physics & getPhysics() const;

    RulesMaster & getRulesMaster();
    const RulesMaster & getRulesMaster() const;

    void movePlayerTo(const StaticMapLayer::size_type x, const StaticMapLayer::size_type y);
    void setStaticObject(const StaticMapLayer::size_type x,
                         const StaticMapLayer::size_type y,
                         const StaticObject & object);

private:
    // everything is shard_ptr because rulesMaster and physics
    // use threading, which sometimes may lead to confusing objects ownership
    std::shared_ptr<StaticMapLayer> staticMapLayer;
    std::shared_ptr<DynamicMapLayer> dynamicMapLayer;
    std::shared_ptr<RulesMaster> rulesMaster;
    std::shared_ptr<Physics> physics;
};

#endif // MAP_H
