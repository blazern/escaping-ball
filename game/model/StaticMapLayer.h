#ifndef STATICMAPLAYER_H
#define STATICMAPLAYER_H

#include <vector>
#include "StaticObject.h"

using std::vector;
class StaticMapLayerListener;

class StaticMapLayer final
{
    friend class Map;
    friend class RulesMaster;
public:
    typedef vector<StaticObject>::size_type size_type;
    typedef std::pair<size_type, size_type> Point;

    static size_type getIndexFor(const float value);
    static vector<Point> getPointsOccupiedBy(const float x, const float y);

public:
    explicit StaticMapLayer();
    StaticMapLayer(const StaticMapLayer & other) = delete;
    StaticMapLayer & operator=(const StaticMapLayer & other) = delete;

    void addListener(StaticMapLayerListener & listener);

    const StaticObject & getObject(const size_type x,
                                   const size_type y) const;

    size_type getWidth() const;
    size_type getHeight() const;

    bool isOutOfBounds(const float x, const float y) const;

    // for friends
private:
    void setObject(const size_type x, const size_type y, const StaticObject & object);

private:
    static const size_type SIZE = 25;

    vector<vector<StaticObject>> objects;
    vector<StaticMapLayerListener *> listeners;
};

#endif // STATICMAPLAYER_H
