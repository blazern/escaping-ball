#ifndef SHOTMOVEMENTSTRATEGY_H
#define SHOTMOVEMENTSTRATEGY_H

#include "DynamicObjectMovementStrategy.h"

class ShotMovementStrategy final : public DynamicObjectMovementStrategy
{
public:
    explicit ShotMovementStrategy() = default;
    ShotMovementStrategy(const ShotMovementStrategy &) = delete;
    ShotMovementStrategy & operator=(const ShotMovementStrategy &) = delete;

    virtual Mover::Direction onPositionReached(const StaticMapLayer::size_type x,
                                               const StaticMapLayer::size_type y) final override;
    virtual Mover::Direction onMetObstacle(const StaticMapLayer::size_type currentX,
                                           const StaticMapLayer::size_type currentY) final override;

    virtual void onStart() final override;
};

#endif // SHOTMOVEMENTSTRATEGY_H
