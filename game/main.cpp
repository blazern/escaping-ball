#include <cstdlib>
#include <time.h>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QQuickWindow>

#include "view/cpp/QmlMapInterface.h"
#include "view/cpp/QmlStaticObjectWrapper.h"
#include "view/cpp/QmlDynamicObjectWrapper.h"
#include "view/cpp/CPPConstants.h"
#include "view/cpp/PlayerIntentionsDeliver.h"
#include "view/cpp/CPPGlobalEventsDeliver.h"
#include "Game.h"

template <typename T> T * getQQuickItem(QQmlApplicationEngine & engine, const QString & itemName);

int main(int argc, char *argv[])
{
    srand(time(nullptr));

    qmlRegisterType<QmlMapInterface>("EscapingBall", 1, 0, "QmlMapInterface");
    qmlRegisterType<QmlStaticObjectWrapper>("EscapingBall", 1, 0, "QmlStaticObjectWrapper");
    qmlRegisterType<QmlDynamicObjectWrapper>("EscapingBall", 1, 0, "QmlDynamicObjectWrapper");
    qmlRegisterType<CPPConstants>("EscapingBall", 1, 0, "CPPConstants");
    qmlRegisterType<PlayerIntentionsDeliver>("EscapingBall", 1, 0, "PlayerIntentionsDeliver");
    qmlRegisterType<CPPGlobalEventsDeliver>("EscapingBall", 1, 0, "CPPGlobalEventsDeliver");

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:///view/qml/main.qml")));

    Game game(engine);

    return app.exec();
}

template <typename T>
T * getQQuickItem(QQmlApplicationEngine & engine, const QString & itemName)
{
    for (auto & rootObject : engine.rootObjects())
    {
        T * const requestedItem = rootObject->findChild<T *>(itemName);
        if (requestedItem != nullptr)
        {
            return requestedItem;
        }
    }
    return nullptr;
}
