#include "Game.h"

#include <stdexcept>

template <typename T> T * getQQuickItem(QQmlApplicationEngine & engine, const QString & itemName);

Game::Game(QQmlApplicationEngine & engine, QObject * parent) :
    QObject(parent),
    qmlMapInterface(getQQuickItem<QmlMapInterface>(engine, "qmlMapInterface")),
    playerIntentionsDeliver(getQQuickItem<PlayerIntentionsDeliver>(engine, "playerIntentionsDeliver")),
    cppGlobalEventsDeliver(getQQuickItem<CPPGlobalEventsDeliver>(engine, "cppGlobalEventsDeliver")),
    map(new Map())
{
    if (qmlMapInterface == nullptr
            || playerIntentionsDeliver == nullptr
            || cppGlobalEventsDeliver == nullptr)
    {
        throw std::runtime_error("could not receive needed object(s) from QML");
    }

    connect(cppGlobalEventsDeliver, &CPPGlobalEventsDeliver::restartRequired,
            this, &Game::onRestartRequired);
    connect(cppGlobalEventsDeliver, &CPPGlobalEventsDeliver::applicationTerminating,
            this, &Game::onApplicationTerminating);

    qmlMapInterface->setMap(*map);
    playerIntentionsDeliver->setMap(*map);
    cppGlobalEventsDeliver->setMap(*map);
}

template <typename T>
T * getQQuickItem(QQmlApplicationEngine & engine,
                  const QString & itemName)
{
    for (auto & rootObject : engine.rootObjects())
    {
        T * const requestedItem = rootObject->findChild<T *>(itemName);
        if (requestedItem != nullptr)
        {
            return requestedItem;
        }
    }
    return nullptr;
}

Game::~Game()
{
    terminate();
    delete map;
}

void Game::terminate()
{
    if (map != nullptr)
    {
        map->getPhysics().terminate();
        map->getRulesMaster().terminate();
    }
}

void Game::onRestartRequired()
{
    Map * const oldMap = map;
    map = new Map();

    qmlMapInterface->setMap(*map);
    playerIntentionsDeliver->setMap(*map);
    cppGlobalEventsDeliver->setMap(*map);

    delete oldMap;
}

void Game::onApplicationTerminating()
{
    terminate();
}

