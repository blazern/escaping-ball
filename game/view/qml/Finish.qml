import QtQuick 2.0
import EscapingBall 1.0

Rectangle {
    id: finish
    width: sizes.side_size
    height: sizes.side_size
    color: "red"
    transform: Rotation { angle: 45 }

    property int xIndex: 0
    property int yIndex: 0
    property PlayerIntentionsDeliver playerIntentionsDeliver: PlayerIntentionsDeliver {}

    function setCoordinates(newX, newY, mapWidth, mapHeight) {
        var xyz = coordinator.calculateXYZ(newX, newY, mapWidth, mapHeight);
        x = xyz[0];
        y = xyz[1];
        z = 10;
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton
        onPressed: {
            finish.color = "blue"
        }
        onReleased: {
            finish.color = "red"
        }
        onClicked: {
            playerIntentionsDeliver.playerWantsMoveTo(xIndex, yIndex);
        }
    }

    Rectangle {
        width: parent.width - 2
        height: parent.height - 2
        color: "orange"
        x: 1
        y: 1

        Rectangle {
            width: parent.width - 2
            height: parent.height - 2
            color: "yellow"
            x: 1
            y: 1

            Rectangle {
                width: parent.width - 2
                height: parent.height - 2
                color: "green"
                x: 1
                y: 1

                Rectangle {
                    width: parent.width - 2
                    height: parent.height - 2
                    color: "blue"
                    x: 1
                    y: 1

                    Rectangle {
                        width: parent.width - 2
                        height: parent.height - 2
                        color: "purple"
                        x: 1
                        y: 1
                    }
                }
            }
        }
    }

    Sizes {
        id: sizes
    }

    MapObjectCoordinator {
        id: coordinator
    }
}
