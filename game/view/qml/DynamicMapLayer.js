var objects = new Array()

function size() {
    return objects.length;
}

function get(index) {
    return objects[index];
}

function add(item) {
    objects.push(item);
}

function destroy() {
    for (var index = 0; index < objects.length; index++) {
        objects[index].destroy();
    }
    objects = [];
}

function remove(index) {
    objects[index].destroy();
    objects.splice(index, 1);
}
