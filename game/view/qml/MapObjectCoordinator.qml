import QtQuick 2.0

Item {
    function calculateXYZ(xLogical, yLogical, mapWidth, mapHeight) {
        var offsetSize = sizes.diagonal_size / 2;
        var xIndex = xLogical / sizes.side_size;
        var yIndex = yLogical / sizes.side_size;
        var x = xIndex * offsetSize - yIndex * offsetSize;
        var y = yIndex * offsetSize + xIndex * offsetSize - mapHeight / 2 - 100;
        var z = (xLogical + sizes.side_size) * (yLogical + sizes.side_size) * 10;
        return [x, y, z];
    }

    Sizes {
        id: sizes
    }
}
