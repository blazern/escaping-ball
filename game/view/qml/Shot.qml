import QtQuick 2.0
import EscapingBall 1.0

Rectangle {
    id: shot
    width: sizes.side_size / 1.5
    height: sizes.side_size / 1.5
    color: "black"
    radius: sizes.side_size * 0.5
    property QmlDynamicObjectWrapper dynamicObjectWrapper: QmlDynamicObjectWrapper {}

    function setCoordinates(newX, newY, mapWidth, mapHeight) {
        var xyz = coordinator.calculateXYZ(newX, newY, mapWidth, mapHeight);
        x = xyz[0] - sizes.side_size / 2;
        y = xyz[1];
        z = xyz[2] * 10;
    }

    Rectangle {
        id: inner
        width: shot.width - 2
        height: shot.width - 2
        x: 1
        y: 1
        anchors.centerIn: shot
        color: "purple"
        radius: width * 0.5
    }

    Sizes {
        id: sizes
    }

    MapObjectCoordinator {
        id: coordinator
    }
}
