import QtQuick 2.0
import EscapingBall 1.0

Rectangle {
    id: patrol
    width: sizes.side_size
    height: sizes.side_size
    color: "red"
    radius: sizes.side_size * 0.5
    property QmlDynamicObjectWrapper dynamicObjectWrapper: QmlDynamicObjectWrapper {}

    function setCoordinates(newX, newY, mapWidth, mapHeight) {
        var xyz = coordinator.calculateXYZ(newX, newY, mapWidth, mapHeight);
        x = xyz[0] - sizes.side_size / 2;
        y = xyz[1];
        z = xyz[2];
    }

    Sizes {
        id: sizes
    }

    MapObjectCoordinator {
        id: coordinator
    }
}
