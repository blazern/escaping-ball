import QtQuick 2.0
import EscapingBall 1.0

Cube {
    id: cannon
    size: 20
    mainColor: "purple"
    acceptedMouseButtons: 0

    property int xIndex: 0
    property int yIndex: 0
    property PlayerIntentionsDeliver playerIntentionsDeliver: PlayerIntentionsDeliver {}

    function setCoordinates(newX, newY, mapWidth, mapHeight) {
        var xyz = coordinator.calculateXYZ(newX, newY, mapWidth, mapHeight);
        x = xyz[0];
        y = xyz[1];
        z = xyz[2];
    }

    Rectangle {
        width: sizes.side_size / 4
        height: sizes.side_size / 2
        radius: width
        color: "black"
        z: 1000
        x: -10
        y: 5
    }

    Sizes {
        id: sizes
    }

    MapObjectCoordinator {
        id: coordinator
    }
}
