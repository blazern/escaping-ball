import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import EscapingBall 1.0
import "StaticMapLayer.js" as StaticMapLayer
import "DynamicMapLayer.js" as DynamicMapLayer

Window {
    id: window
    visible: true
    width: 1024
    height: 768
    property PlayerIntentionsDeliver playerIntentionsDeliver: PlayerIntentionsDeliver {
        id: playerIntentionsDeliver
        objectName: "playerIntentionsDeliver"
    }

    Component.onDestruction: {
        cppGlobalEventsDeliver.onApplicationTerminating();
    }

    Text {
        id: finishGameMessage
        anchors.centerIn: parent
        font.pointSize: 50
        color: "blue"
        z: 3
    }

    Button {
        id: restartButton
        anchors.top: finishGameMessage.bottom
        anchors.horizontalCenter: finishGameMessage.horizontalCenter
        text: "Начать заново"
        visible: false
        z: 3

        onClicked: {
            cppGlobalEventsDeliver.onRestartRequired();
        }
    }

    CPPGlobalEventsDeliver {
        id: cppGlobalEventsDeliver
        objectName: "cppGlobalEventsDeliver"
        onGameFinished: {
            if (finishReason == constants.WIN) {
                finishGameMessage.text = "ПОБЕДА";
                finishGameMessage.visible = true;
            } else if (finishReason == constants.DEATH) {
                finishGameMessage.text = "ПОРАЖЕНИЕ";
                finishGameMessage.visible = true;
            }
            restartButton.visible = true;
        }
    }

    QmlMapInterface {
        id: qmlMapInterface
        objectName: "qmlMapInterface"

        ObjectsCreator {
            id: objectsCreator
        }

        onMapWasSet: {
            finishGameMessage.visible = false;
            restartButton.visible = false;
            DynamicMapLayer.destroy();
            StaticMapLayer.destroy();

            objectsCreator.mapWidth = qmlMapInterface.getStaticLayerWidth();
            objectsCreator.mapHeight = qmlMapInterface.getStaticLayerHeight();

            StaticMapLayer.initialize(
                        qmlMapInterface.getStaticLayerWidth(),
                        qmlMapInterface.getStaticLayerHeight());

            for (var x = 0; x < qmlMapInterface.getStaticLayerWidth(); x++) {
                for (var y = 0; y < qmlMapInterface.getStaticLayerHeight(); y++) {
                    var cppStaticObject = qmlMapInterface.getStaticObject(x, y);
                    StaticMapLayer.put(
                                objectsCreator.createStaticElementFrom(cppStaticObject), x, y)
                }
            }

            for (var index = 0; index < qmlMapInterface.getDynamicObjectsCount(); index++) {
                var cppDynamicObject = qmlMapInterface.getDynamicObject(index);
                DynamicMapLayer.add(
                            objectsCreator.createDynamicObjectFrom(cppDynamicObject));
            }
        }

        onDynamicObjectCreated: {
            DynamicMapLayer.add(
                        objectsCreator.createDynamicObjectFrom(cppDynamicObject));
        }

        onDynamicObjectDestroyed: {
            for (var index = 0; index < DynamicMapLayer.size(); index++) {
                var dynamicElement = DynamicMapLayer.get(index);
                if (dynamicElement.dynamicObjectWrapper === cppDynamicObject) {
                    DynamicMapLayer.remove(index);
                    return;
                }
            }
        }

        onDynamicObjectChangedPosition: {
            for (var index = 0; index < DynamicMapLayer.size(); index++) {
                var dynamicElement = DynamicMapLayer.get(index);
                if (dynamicElement.dynamicObjectWrapper === cppDynamicObject) {
                    dynamicElement.setCoordinates(
                                cppDynamicObject.x * sizes.side_size,
                                cppDynamicObject.y * sizes.side_size,
                                qmlMapInterface.getStaticLayerWidth() * sizes.side_size,
                                qmlMapInterface.getStaticLayerHeight() * sizes.side_size);
                }
            }
        }

        onStaticObjectChanged: {
            var x = cppStaticObject.x;
            var y = cppStaticObject.y;

            StaticMapLayer.get(x, y).destroy();
            var createdObject = objectsCreator.createStaticElementFrom(cppStaticObject);

            StaticMapLayer.put(createdObject, x, y);
        }
    }

    Rectangle {
        id: rectangleForMapObjects
        anchors.centerIn: parent
        z: 1
    }

    Sizes {
        id: sizes
    }

    CPPConstants {
        id: constants
    }
}
