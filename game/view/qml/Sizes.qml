import QtQuick 2.0

Item {
    property int side_size: 20
    property int diagonal_size: Math.sqrt(Math.pow(side_size, 2) * 2)
}
