var objects = new Array()

function initialize(width, height) {
    for (var x = 0; x < width; x++) {
        objects.push(new Array());
        for (var y = 0; y < height; y++) {
            objects[x].push(null);
        }
    }
}

function get(x, y) {
    return objects[x][y];
}

function put(item, x, y) {
    objects[x][y] = item;
}

function destroy() {
    for (var x = 0; x < objects.length; x++) {
        for (var y = 0; y < objects[x].length; y++) {
            objects[x][y].destroy();
        }
    }
    objects = [];
}

