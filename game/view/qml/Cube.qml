import QtQuick 2.0
import EscapingBall 1.0

Item {
    id: cube
    property color mainColor: "blue"
    property color linesColor: "red"
    property color clickColor: "black"
    property int size: 7
    property int acceptedMouseButtons: Qt.LeftButton | Qt.RightButton
    signal clicked(variant mouse)

    MouseArea {
        anchors.fill: parent
        acceptedButtons: acceptedMouseButtons
        onPressed: {
            innerTop.color = clickColor
        }
        onReleased: {
            innerTop.color = mainColor
        }
        onClicked: {
            cube.clicked(mouse);
        }
    }

    Rectangle {
        id: top
        width: sizes.side_size
        height: sizes.side_size
        y: -size
        z: middle.z + 1
        color: linesColor
        transform: Rotation { angle: 45 }

        Rectangle {
            id: innerTop
            width: top.width - 2
            height: top.height - 2
            color: mainColor
            x: 1
            y: 1
        }

        MouseArea {
            anchors.fill: parent
            acceptedButtons: acceptedMouseButtons
            onPressed: {
                innerTop.color = clickColor
            }
            onReleased: {
                innerTop.color = mainColor
            }
            onClicked: {
                cube.clicked(mouse);
            }
        }
    }

    Rectangle {
        id: middle
        width: sizes.diagonal_size
        height: bottom.y - top.y
        x: top.x - sizes.diagonal_size / 2
        y: top.y + sizes.diagonal_size / 2
        z: bottom.z + 1
        color: innerTop.color

        MouseArea {
            anchors.fill: parent
            acceptedButtons: acceptedMouseButtons
            onPressed: {
                innerTop.color = clickColor
            }
            onReleased: {
                innerTop.color = mainColor
            }
            onClicked: {
                cube.clicked(mouse);
            }
        }
    }

    Rectangle {
        id: centerLine
        width: 1
        height: middle.height
        x: top.x - width / 2
        y: top.y + sizes.diagonal_size
        color: top.color
        z: bottom.z + 1
    }

    Rectangle {
        id: leftLine
        width: 1
        height: middle.height
        x: middle.x
        y: middle.y
        color: top.color
        z: centerLine.z
    }

    Rectangle {
        id: rightLine
        width: 1
        height: middle.height
        x: middle.x + middle.width - width
        y: middle.y
        color: top.color
        z: centerLine.z
    }

    Rectangle {
        id: bottom
        width: sizes.side_size
        height: sizes.side_size
        z: 0
        color: innerTop.color
        transform: Rotation { angle: 45 }

        MouseArea {
            anchors.fill: parent
            acceptedButtons: acceptedMouseButtons
            onPressed: {
                innerTop.color = clickColor
            }
            onReleased: {
                innerTop.color = mainColor
            }
            onClicked: {
                cube.clicked(mouse);
            }
        }
    }

    Sizes {
        id: sizes
    }
}
