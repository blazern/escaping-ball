import QtQuick 2.0
import EscapingBall 1.0

Rectangle {
    id: tile
    width: sizes.side_size
    height: sizes.side_size
    color: "red"
    transform: Rotation { angle: 45 }

    property int xIndex: 0
    property int yIndex: 0
    property color innerColor: "grey"
    property PlayerIntentionsDeliver playerIntentionsDeliver: PlayerIntentionsDeliver {}

    function setCoordinates(newX, newY, mapWidth, mapHeight) {
        var xyz = coordinator.calculateXYZ(newX, newY, mapWidth, mapHeight);
        x = xyz[0];
        y = xyz[1];
        z = 0;
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onPressed: {
            if (mouse.button == Qt.LeftButton) {
                innerColor = "blue"
            } else if (mouse.button == Qt.RightButton) {
                innerColor = "black"
            }
        }
        onReleased: {
            innerColor = "grey"
        }
        onClicked: {
            if (mouse.button == Qt.LeftButton) {
                playerIntentionsDeliver.playerWantsMoveTo(xIndex, yIndex);
            } else if (mouse.button == Qt.RightButton) {
                playerIntentionsDeliver.playerWantsPlaceObstacleAt(xIndex, yIndex);
            }
        }
    }

    Rectangle {
        width: parent.width - 2
        height: parent.height - 2
        color: innerColor
    }

    Sizes {
        id: sizes
    }

    MapObjectCoordinator {
        id: coordinator
    }
}
