import QtQuick 2.0
import EscapingBall 1.0

Cube {
    id: obstacle
    acceptedMouseButtons: Qt.RightButton
    property int xIndex: 0
    property int yIndex: 0
    property PlayerIntentionsDeliver playerIntentionsDeliver: PlayerIntentionsDeliver {}

    function setCoordinates(newX, newY, mapWidth, mapHeight) {
        var xyz = coordinator.calculateXYZ(newX, newY, mapWidth, mapHeight);
        x = xyz[0];
        y = xyz[1];
        z = xyz[2];
    }

    onClicked: {
        if (mouse.button === Qt.RightButton) {
            playerIntentionsDeliver.playerWantsRemoveObstacleFrom(xIndex, yIndex);
        }
    }

    MapObjectCoordinator {
        id: coordinator
    }
}
