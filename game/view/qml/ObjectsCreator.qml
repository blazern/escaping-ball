import QtQuick 2.0

Item {
    property int mapWidth: 0
    property int mapHeight: 0

    function createStaticElementFrom(cppStaticMapObject) {
        if (cppStaticMapObject.type === constants.TILE) {
            return privateData.createTileFrom(cppStaticMapObject);
        } else if (cppStaticMapObject.type === constants.FINISH) {
            return privateData.createFinishFrom(cppStaticMapObject);
        } else if (cppStaticMapObject.type === constants.CANNON) {
            return privateData.createCannonFrom(cppStaticMapObject);
        } else {
            return privateData.createObstacleFrom(cppStaticMapObject);
        }
    }

    function createDynamicObjectFrom(cppDynamicObject) {
        if (cppDynamicObject.type === constants.PLAYER) {
            return privateData.createPlayerFrom(cppDynamicObject);
        } else if (cppDynamicObject.type === constants.PATROL) {
            return privateData.createPatrolFrom(cppDynamicObject);
        } else if (cppDynamicObject.type === constants.SHOT) {
            return privateData.createShotFrom(cppDynamicObject);
        }
    }

    Item {
        id: privateData

        function createTileFrom(cppStaticMapObject) {
            var component = Qt.createComponent("Tile.qml");
            if (component.status === Component.Ready) {
                var element = component.createObject(rectangleForMapObjects);
                setUpQmlStaticObjectByCppStaticObject(element, cppStaticMapObject);
                return element;
            }
        }

        function setUpQmlStaticObjectByCppStaticObject(qmlStaticObject, cppStaticObject) {
            qmlStaticObject.xIndex = cppStaticObject.x;
            qmlStaticObject.yIndex = cppStaticObject.y;
            qmlStaticObject.playerIntentionsDeliver = playerIntentionsDeliver;
            qmlStaticObject.setCoordinates(
                        cppStaticObject.x * sizes.side_size,
                        cppStaticObject.y * sizes.side_size,
                        mapWidth * sizes.side_size,
                        mapHeight * sizes.side_size);
        }

        function createObstacleFrom(cppStaticMapObject) {
            var component = Qt.createComponent("Obstacle.qml");
            if (component.status === Component.Ready) {
                var element = component.createObject(rectangleForMapObjects);
                setUpQmlStaticObjectByCppStaticObject(element, cppStaticMapObject);
                return element;
            }
        }

        function createFinishFrom(cppStaticMapObject) {
            var component = Qt.createComponent("Finish.qml");
            if (component.status === Component.Ready) {
                var element = component.createObject(rectangleForMapObjects);
                setUpQmlStaticObjectByCppStaticObject(element, cppStaticMapObject);
                return element;
            }
        }

        function createCannonFrom(cppStaticMapObject) {
            var component = Qt.createComponent("Cannon.qml");
            if (component.status === Component.Ready) {
                var element = component.createObject(rectangleForMapObjects);
                setUpQmlStaticObjectByCppStaticObject(element, cppStaticMapObject);
                return element;
            }
        }

        function createPlayerFrom(cppDynamicObject) {
            var component = Qt.createComponent("Player.qml");
            if (component.status === Component.Ready) {
                var element = component.createObject(rectangleForMapObjects);
                setUpQmlDynamicObjectByCppDynamicObject(element, cppDynamicObject);
                return element;
            }
        }

        function setUpQmlDynamicObjectByCppDynamicObject(qmlStaticObject, cppDynamicObject) {
            qmlStaticObject.setCoordinates(
                        cppDynamicObject.x * sizes.side_size,
                        cppDynamicObject.y * sizes.side_size,
                        mapWidth * sizes.side_size,
                        mapHeight * sizes.side_size);
            qmlStaticObject.dynamicObjectWrapper = cppDynamicObject;
        }

        function createPatrolFrom(cppDynamicObject) {
            var component = Qt.createComponent("Patrol.qml");
            if (component.status === Component.Ready) {
                var element = component.createObject(rectangleForMapObjects);
                setUpQmlDynamicObjectByCppDynamicObject(element, cppDynamicObject);
                return element;
            }
        }

        function createShotFrom(cppDynamicObject) {
            var component = Qt.createComponent("Shot.qml");
            if (component.status === Component.Ready) {
                var element = component.createObject(rectangleForMapObjects);
                setUpQmlDynamicObjectByCppDynamicObject(element, cppDynamicObject);
                return element;
            }
        }
    }

    Sizes {
        id: sizes
    }
}
