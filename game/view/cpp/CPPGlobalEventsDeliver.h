#ifndef CPPGLOBALEVENTSDELIVER_H
#define CPPGLOBALEVENTSDELIVER_H

#include <QQuickItem>
#include "../../model/Map.h"
#include "../../model/RulesMasterListener.h"

class CPPGlobalEventsDeliver : public QQuickItem, public RulesMasterListener
{
    Q_OBJECT
public:
    explicit CPPGlobalEventsDeliver(QQuickItem * parent = 0);

    Q_INVOKABLE void onRestartRequired();
    Q_INVOKABLE void onApplicationTerminating();

    void setMap(Map & map);

    virtual void onGameFinished(const RulesMaster::GameFinishReason finishReason) final override;

signals:
    void gameFinished(int finishReason);
    void restartRequired();
    void applicationTerminating();
};

#endif // CPPGLOBALEVENTSDELIVER_H
