#ifndef PLAYERINTENTIONSDELIVER_H
#define PLAYERINTENTIONSDELIVER_H

#include <QQuickItem>
#include "../../model/Map.h"

class PlayerIntentionsDeliver : public QQuickItem
{
    Q_OBJECT
public:
    explicit PlayerIntentionsDeliver(QQuickItem * parent = 0);

    Q_INVOKABLE void playerWantsMoveTo(const int x, const int y);
    Q_INVOKABLE void playerWantsPlaceObstacleAt(const int x, const int y);
    Q_INVOKABLE void playerWantsRemoveObstacleFrom(const int x, const int y);

    void setMap(Map & map);

private:
    Map * map;
};

#endif // PLAYERINTENTIONSDELIVER_H
