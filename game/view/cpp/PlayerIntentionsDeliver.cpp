#include "PlayerIntentionsDeliver.h"

#include "../../model/StaticMapLayer.h"

PlayerIntentionsDeliver::PlayerIntentionsDeliver(QQuickItem * parent) :
    QQuickItem(parent),
    map(nullptr)
{
}

void PlayerIntentionsDeliver::playerWantsMoveTo(const int x, const int y)
{
    if (map != nullptr)
    {
        map->movePlayerTo(
                    static_cast<StaticMapLayer::size_type>(x),
                    static_cast<StaticMapLayer::size_type>(y));
    }
}

void PlayerIntentionsDeliver::playerWantsPlaceObstacleAt(const int x, const int y)
{
    if (map != nullptr)
    {
        map->setStaticObject(x, y, StaticObject(StaticObject::Type::OBSTACLE));
    }
}

void PlayerIntentionsDeliver::playerWantsRemoveObstacleFrom(const int x, const int y)
{
    if (map != nullptr)
    {
        map->setStaticObject(x, y, StaticObject(StaticObject::Type::TILE));
    }
}

void PlayerIntentionsDeliver::setMap(Map & map)
{
    this->map = &map;
}
