#include "QmlMapInterface.h"

#include <algorithm>

QmlMapInterface::QmlMapInterface(QQuickItem * parent) :
    QQuickItem(parent),
    map(nullptr),
    staticObjectsWrappers(),
    dynamicObjectsWrappers(),
    dynamicObjectsMutex()
{
    qRegisterMetaType<PositionedDynamicObject *>("PositionedDynamicObjectReference");

    connect(this, &QmlMapInterface::dynamicObjectCreatedInOtherThread,
            this, &QmlMapInterface::onDynamicObjectCreatedInOtherThread);

    connect(this, &QmlMapInterface::dynamicObjectDestroyedInOtherThread,
            this, &QmlMapInterface::onDynamicObjectDestroyedInOtherThread);
}

int QmlMapInterface::getStaticLayerWidth() const
{
    if (map != nullptr)
    {
        return static_cast<int>(map->getStaticLayer().getWidth());
    }
    else
    {
        return 0;
    }
}

int QmlMapInterface::getStaticLayerHeight() const
{
    if (map != nullptr)
    {
        return static_cast<int>(map->getStaticLayer().getHeight());
    }
    else
    {
        return 0;
    }
}

QmlStaticObjectWrapper * QmlMapInterface::getStaticObject(const int x, const int y)
{
    return staticObjectsWrappers[x][y].data();
}

int QmlMapInterface::getDynamicObjectsCount() const
{
    return dynamicObjectsWrappers.size();
}

QmlDynamicObjectWrapper * QmlMapInterface::getDynamicObject(const int index)
{
    if (index < dynamicObjectsWrappers.size())
    {
        return dynamicObjectsWrappers[index].data();
    }
    else
    {
        return dynamicObjectsWrappers[0].data();
    }
}

void QmlMapInterface::setMap(Map & map)
{
    staticObjectsWrappers.clear();
    dynamicObjectsWrappers.clear();

    this->map = &map;
    staticObjectsWrappers.clear();
    fillStaticObjectsWrappers();
    fillDynamicObjectsWrappers();
    map.getPhysics().addListener(*this);
    map.getStaticLayer().addListener(*this);
    map.getDynamicLayer().addListener(*this);
    emit mapWasSet();
}

void QmlMapInterface::fillStaticObjectsWrappers()
{
    const StaticMapLayer & staticLayer = map->getStaticLayer();

    for (StaticMapLayer::size_type x = 0; x < staticLayer.getWidth(); x++)
    {
        staticObjectsWrappers.append(QVector<QSharedPointer<QmlStaticObjectWrapper>>());
        for (StaticMapLayer::size_type y = 0; y < staticLayer.getHeight(); y++)
        {
            staticObjectsWrappers[x].append(
                        QSharedPointer<QmlStaticObjectWrapper>(
                            new QmlStaticObjectWrapper(
                                staticLayer.getObject(x, y),
                                static_cast<int>(x),
                                static_cast<int>(y),
                                this)));
        }
    }
}

void QmlMapInterface::fillDynamicObjectsWrappers()
{
    std::lock_guard<std::mutex> mutexLock(dynamicObjectsMutex);

    const DynamicMapLayer & dynamicLayer = map->getDynamicLayer();
    for (const auto & dynamicObject : dynamicLayer.getObjectsCopy())
    {
        dynamicObjectsWrappers.push_back(
                    QSharedPointer<QmlDynamicObjectWrapper>(
                        new QmlDynamicObjectWrapper(*dynamicObject, this)));
    }
}

void QmlMapInterface::onDynamicObjectMoved(const PositionedDynamicObject & object)
{
    std::lock_guard<std::mutex> mutexLock(dynamicObjectsMutex);

    for (auto & dynamicObjectWrapper : dynamicObjectsWrappers)
    {
        if (&(dynamicObjectWrapper->getObject()) == &object)
        {
            dynamicObjectsMutex.unlock();
            emit dynamicObjectChangedPosition(dynamicObjectWrapper.data());
            return;
        }
    }
}

void QmlMapInterface::onStaticObjectChanged(const StaticMapLayer::size_type x,
                                            const StaticMapLayer::size_type y,
                                            const StaticObject &)
{
    emit staticObjectChanged(staticObjectsWrappers[x][y].data());
}

void QmlMapInterface::onObjectCreated(PositionedDynamicObject & object)
{
    emit dynamicObjectCreatedInOtherThread(&object);
}

void QmlMapInterface::onObjectDestroyed(PositionedDynamicObject & object)
{
    emit dynamicObjectDestroyedInOtherThread(&object);
}

void QmlMapInterface::onDynamicObjectCreatedInOtherThread(PositionedDynamicObject * object)
{
    std::lock_guard<std::mutex> mutexLock(dynamicObjectsMutex);

    dynamicObjectsWrappers.push_back(
                QSharedPointer<QmlDynamicObjectWrapper>(
                    new QmlDynamicObjectWrapper(*object, this)));

    dynamicObjectsMutex.unlock();

    emit dynamicObjectCreated(dynamicObjectsWrappers.last().data());
}

void QmlMapInterface::onDynamicObjectDestroyedInOtherThread(PositionedDynamicObject * object)
{
    std::lock_guard<std::mutex> mutexLock(dynamicObjectsMutex);

    auto foundItemIterator = std::find_if(
                dynamicObjectsWrappers.begin(),
                dynamicObjectsWrappers.end(),
                [object] (QSharedPointer<QmlDynamicObjectWrapper> & wrapper) {
        return object == &(wrapper->getObject());
    });

    if (foundItemIterator != dynamicObjectsWrappers.end())
    {
        QSharedPointer<QmlDynamicObjectWrapper> foundItem(*foundItemIterator);
        dynamicObjectsWrappers.erase(foundItemIterator);

        dynamicObjectsMutex.unlock();
        emit dynamicObjectDestroyed(foundItem.data());
    }
}
