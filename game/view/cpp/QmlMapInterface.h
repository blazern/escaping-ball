#ifndef QMLMAPINTERFACE_H
#define QMLMAPINTERFACE_H

#include <QSharedPointer>
#include <QVector>
#include <QQuickItem>
#include <mutex>
#include "QmlStaticObjectWrapper.h"
#include "QmlDynamicObjectWrapper.h"
#include "../../model/Map.h"
#include "../../model/PhysicsListener.h"
#include "../../model/StaticMapLayerListener.h"
#include "../../model/DynamicMapLayerListener.h"

class QmlMapInterface :
        public QQuickItem,
        public PhysicsListener,
        public StaticMapLayerListener,
        public DynamicMapLayerListener
{
    Q_OBJECT
    Q_DISABLE_COPY(QmlMapInterface)
public:
    Q_INVOKABLE int getStaticLayerWidth() const;
    Q_INVOKABLE int getStaticLayerHeight() const;
    Q_INVOKABLE QmlStaticObjectWrapper * getStaticObject(const int x, const int y);

    Q_INVOKABLE int getDynamicObjectsCount() const;
    Q_INVOKABLE QmlDynamicObjectWrapper * getDynamicObject(const int index);

    explicit QmlMapInterface(QQuickItem * parent = 0);

    void setMap(Map & map);

    virtual void onDynamicObjectMoved(const PositionedDynamicObject & object) final override;
    virtual void onStaticObjectChanged(const StaticMapLayer::size_type x,
                                       const StaticMapLayer::size_type y,
                                       const StaticObject & object) final override;

    virtual void onObjectCreated(PositionedDynamicObject & object) final override;
    virtual void onObjectDestroyed(PositionedDynamicObject & object) final override;

signals:
    void mapWasSet();
    void dynamicObjectChangedPosition(QmlDynamicObjectWrapper * cppDynamicObject);
    void dynamicObjectCreated(QmlDynamicObjectWrapper * cppDynamicObject);
    void dynamicObjectDestroyed(QmlDynamicObjectWrapper * cppDynamicObject);
    void staticObjectChanged(QmlStaticObjectWrapper * cppStaticObject);

signals:
    void dynamicObjectCreatedInOtherThread(PositionedDynamicObject * object);
    void dynamicObjectDestroyedInOtherThread(PositionedDynamicObject * object);

private slots:
    void onDynamicObjectCreatedInOtherThread(PositionedDynamicObject * object);
    void onDynamicObjectDestroyedInOtherThread(PositionedDynamicObject * object);

private:
    void fillStaticObjectsWrappers();
    void fillDynamicObjectsWrappers();

private:
    Map * map;
    QVector<QVector<QSharedPointer<QmlStaticObjectWrapper>>> staticObjectsWrappers;
    QVector<QSharedPointer<QmlDynamicObjectWrapper>> dynamicObjectsWrappers;
    std::mutex dynamicObjectsMutex;
};

#endif // QMLMAPINTERFACE_H
