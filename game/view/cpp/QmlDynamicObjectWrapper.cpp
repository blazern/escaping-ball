#include "QmlDynamicObjectWrapper.h"

#include <iostream>

QmlDynamicObjectWrapper::QmlDynamicObjectWrapper(QQuickItem * parent) :
    QQuickItem(parent),
    dynamicObject(nullptr)
{
}

QmlDynamicObjectWrapper::QmlDynamicObjectWrapper(
        const PositionedDynamicObject & dynamicObject,
        QQuickItem * parent) :
    QQuickItem(parent),
    dynamicObject(&dynamicObject)
{
}

const PositionedDynamicObject & QmlDynamicObjectWrapper::getObject() const
{
#ifdef DEBUG
    std::cout << "QmlDynamicObjectWrapper::getObject(): for some reason object constructed "
                 "by QML-constructor is asked to give its dynamic object which it can not "
                 "have, the application is probably going to crash";
#endif
    return *dynamicObject;
}

int QmlDynamicObjectWrapper::getType() const
{
    if (dynamicObject != nullptr)
    {
        return static_cast<int>(dynamicObject->getType());
    }
    return DynamicObject::Type::PATROL;
}

qreal QmlDynamicObjectWrapper::getX() const
{
    if (dynamicObject != nullptr)
    {
        return static_cast<qreal>(dynamicObject->getX());
    }
    return 0;
}

qreal QmlDynamicObjectWrapper::getY() const
{
    if (dynamicObject != nullptr)
    {
        return static_cast<qreal>(dynamicObject->getY());
    }
    return 0;
}
