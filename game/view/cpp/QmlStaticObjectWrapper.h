#ifndef QMLSTATICOBJECTWRAPPER_H
#define QMLSTATICOBJECTWRAPPER_H

#include <QQuickItem>

#include "model/StaticObject.h"

class QmlStaticObjectWrapper : public QQuickItem
{
    Q_OBJECT
    Q_DISABLE_COPY(QmlStaticObjectWrapper)
public:
    Q_PROPERTY(int type READ getType CONSTANT FINAL)
    Q_PROPERTY(int x READ getX CONSTANT FINAL)
    Q_PROPERTY(int y READ getY CONSTANT FINAL)

    explicit QmlStaticObjectWrapper(QQuickItem * parent = 0); // required by QML
    explicit QmlStaticObjectWrapper(
            const StaticObject & staticObject,
            const int x,
            const int y,
            QQuickItem * parent = 0);

private:
    int getType() const;
    int getX() const;
    int getY() const;

private:
    const StaticObject * const staticObject;
    const int x;
    const int y;
};

#endif // QMLSTATICOBJECTWRAPPER_H
