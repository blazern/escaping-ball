#include "CPPGlobalEventsDeliver.h"

CPPGlobalEventsDeliver::CPPGlobalEventsDeliver(QQuickItem * parent) :
    QQuickItem(parent)
{
}

void CPPGlobalEventsDeliver::onRestartRequired()
{
    emit restartRequired();
}

void CPPGlobalEventsDeliver::onApplicationTerminating()
{
    emit applicationTerminating();
}

void CPPGlobalEventsDeliver::setMap(Map & map)
{
    map.getRulesMaster().addListener(*this);
}

void CPPGlobalEventsDeliver::onGameFinished(
        const RulesMaster::GameFinishReason finishReason)
{
    emit gameFinished(static_cast<int>(finishReason));
}
