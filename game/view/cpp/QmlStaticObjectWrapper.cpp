#include "QmlStaticObjectWrapper.h"

QmlStaticObjectWrapper::QmlStaticObjectWrapper(QQuickItem * parent) :
    QQuickItem(parent),
    staticObject(nullptr),
    x(0),
    y(0)
{
}

QmlStaticObjectWrapper::QmlStaticObjectWrapper(const StaticObject & staticObject,
                                               const int x,
                                               const int y,
                                               QQuickItem * parent) :
    QQuickItem(parent),
    staticObject(&staticObject),
    x(x),
    y(y)
{
}

int QmlStaticObjectWrapper::getType() const
{
    if (staticObject != nullptr)
    {
        return staticObject->getType();
    }
    return StaticObject::TILE;
}

int QmlStaticObjectWrapper::getX() const
{
    return x;
}

int QmlStaticObjectWrapper::getY() const
{
    return y;
}
