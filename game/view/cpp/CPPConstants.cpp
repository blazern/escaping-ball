#include "CPPConstants.h"

#include "../../model/StaticObject.h"
#include "../../model/DynamicObject.h"
#include "../../model/Mover.h"
#include "../../model/RulesMaster.h"

CPPConstants::CPPConstants(QQuickItem * parent) :
    QQuickItem(parent)
{
}

int CPPConstants::staticObjectEnumValueTile()
{
    return StaticObject::Type::TILE;
}

int CPPConstants::staticObjectEnumValueObstacle()
{
    return StaticObject::Type::OBSTACLE;
}

int CPPConstants::staticObjectEnumValueFinish()
{
    return StaticObject::Type::FINISH;
}

int CPPConstants::staticObjectEnumValueCannon()
{
    return StaticObject::Type::CANNON;
}

int CPPConstants::dynamicObjectEnumValuePlayer()
{
    return DynamicObject::Type::PLAYER;
}

int CPPConstants::dynamicObjectEnumValuePatrol()
{
    return DynamicObject::Type::PATROL;
}

int CPPConstants::dynamicObjectEnumValueShot()
{
    return DynamicObject::Type::SHOT;
}

int CPPConstants::gameFinishReasonWin()
{
    return RulesMaster::GameFinishReason::WIN;
}

int CPPConstants::gameFinishReasonDeath()
{
    return RulesMaster::GameFinishReason::DEATH;
}

int CPPConstants::movementDirectionNone()
{
    return Mover::Direction::NONE;
}

int CPPConstants::movementDirectionYDecrease()
{
    return Mover::Direction::Y_DECREASE;
}

int CPPConstants::movementDirectionXIncrease()
{
    return Mover::Direction::X_INCREASE;
}

int CPPConstants::movementDirectionYIncrease()
{
    return Mover::Direction::Y_INCREASE;
}

int CPPConstants::movementDirectionXDecrease()
{
    return Mover::Direction::X_DECREASE;
}
