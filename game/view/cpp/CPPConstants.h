#ifndef CPPCONSTANTS_H
#define CPPCONSTANTS_H

#include <QQuickItem>

class CPPConstants : public QQuickItem
{
    Q_OBJECT
public:
    explicit CPPConstants(QQuickItem * parent = 0);
    virtual ~CPPConstants() {}

    Q_PROPERTY(int TILE READ staticObjectEnumValueTile CONSTANT FINAL)
    Q_PROPERTY(int OBSTACLE READ staticObjectEnumValueObstacle CONSTANT FINAL)
    Q_PROPERTY(int FINISH READ staticObjectEnumValueFinish CONSTANT FINAL)
    Q_PROPERTY(int CANNON READ staticObjectEnumValueCannon CONSTANT FINAL)

    Q_PROPERTY(int PLAYER READ dynamicObjectEnumValuePlayer CONSTANT FINAL)
    Q_PROPERTY(int PATROL READ dynamicObjectEnumValuePatrol CONSTANT FINAL)
    Q_PROPERTY(int SHOT READ dynamicObjectEnumValueShot CONSTANT FINAL)

    Q_PROPERTY(int WIN READ gameFinishReasonWin CONSTANT FINAL)
    Q_PROPERTY(int DEATH READ gameFinishReasonDeath CONSTANT FINAL)

    Q_PROPERTY(int NONE READ movementDirectionNone CONSTANT FINAL)
    Q_PROPERTY(int Y_DECREASE READ movementDirectionYDecrease CONSTANT FINAL)
    Q_PROPERTY(int X_INCREASE READ movementDirectionXIncrease CONSTANT FINAL)
    Q_PROPERTY(int Y_DECREASE READ movementDirectionYIncrease CONSTANT FINAL)
    Q_PROPERTY(int X_DECRAESE READ movementDirectionXDecrease CONSTANT FINAL)

private:
    static int staticObjectEnumValueTile();
    static int staticObjectEnumValueObstacle();
    static int staticObjectEnumValueFinish();
    static int staticObjectEnumValueCannon();
    static int dynamicObjectEnumValuePlayer();
    static int dynamicObjectEnumValuePatrol();
    static int dynamicObjectEnumValueShot();
    static int gameFinishReasonWin();
    static int gameFinishReasonDeath();
    static int movementDirectionNone();
    static int movementDirectionYDecrease();
    static int movementDirectionXIncrease();
    static int movementDirectionYIncrease();
    static int movementDirectionXDecrease();
};

#endif // CPPCONSTANTS_H
