#ifndef QMLDYNAMICOBJECTWRAPPER_H
#define QMLDYNAMICOBJECTWRAPPER_H

#include <QQuickItem>
#include "../../model/PositionedDynamicObject.h"

class QmlDynamicObjectWrapper : public QQuickItem
{
    Q_OBJECT
    Q_DISABLE_COPY(QmlDynamicObjectWrapper)
public:
    Q_PROPERTY(int type READ getType CONSTANT FINAL)
    Q_PROPERTY(qreal x READ getX FINAL)
    Q_PROPERTY(qreal y READ getY FINAL)

    explicit QmlDynamicObjectWrapper(QQuickItem * parent = 0); // required by QML
    explicit QmlDynamicObjectWrapper(
            const PositionedDynamicObject & dynamicObject,
            QQuickItem * parent = 0);

    const PositionedDynamicObject & getObject() const;

private:
    int getType() const;
    qreal getX() const;
    qreal getY() const;

private:
    const PositionedDynamicObject * const dynamicObject;
};

#endif // QMLDYNAMICOBJECTWRAPPER_H
