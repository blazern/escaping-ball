QMAKE_CXXFLAGS += -std=c++11 -Wall -Wextra

QT += qml quick

HEADERS += \
    $$PWD/model/StaticMapLayer.h \
    $$PWD/model/StaticObject.h \
    $$PWD/view/cpp/QmlMapInterface.h \
    $$PWD/view/cpp/QmlStaticObjectWrapper.h \
    $$PWD/model/Map.h \
    $$PWD/model/DynamicMapLayer.h \
    $$PWD/model/DynamicObject.h \
    $$PWD/model/PositionedDynamicObject.h \
    $$PWD/model/Mover.h \
    $$PWD/model/Physics.h \
    $$PWD/model/DynamicObjectMovement.h \
    $$PWD/model/PlayerMovementStrategy.h \
    $$PWD/model/PhysicsListener.h \
    $$PWD/view/cpp/QmlDynamicObjectWrapper.h \
    $$PWD/view/cpp/CPPConstants.h \
    $$PWD/view/cpp/PlayerIntentionsDeliver.h \
    $$PWD/model/StaticMapLayerListener.h \
    $$PWD/model/RulesMaster.h \
    $$PWD/model/DynamicObjectMovementStrategy.h \
    $$PWD/model/PatrolMovementStrategy.h \
    $$PWD/model/Constants.h \
    $$PWD/model/RulesMasterListener.h \
    $$PWD/view/cpp/CPPGlobalEventsDeliver.h \
    $$PWD/Game.h \
    $$PWD/model/DynamicMapLayerListener.h \
    $$PWD/model/ShotMovementStrategy.h

SOURCES += \
    $$PWD/model/StaticMapLayer.cpp \
    $$PWD/model/StaticObject.cpp \
    $$PWD/view/cpp/QmlMapInterface.cpp \
    $$PWD/view/cpp/QmlStaticObjectWrapper.cpp \
    $$PWD/model/Map.cpp \
    $$PWD/model/DynamicMapLayer.cpp \
    $$PWD/model/PositionedDynamicObject.cpp \
    $$PWD/model/Physics.cpp \
    $$PWD/model/DynamicObjectMovement.cpp \
    $$PWD/model/PlayerMovementStrategy.cpp \
    $$PWD/view/cpp/QmlDynamicObjectWrapper.cpp \
    $$PWD/view/cpp/CPPConstants.cpp \
    $$PWD/view/cpp/PlayerIntentionsDeliver.cpp \
    $$PWD/model/RulesMaster.cpp \
    $$PWD/model/DynamicObject.cpp \
    $$PWD/model/PatrolMovementStrategy.cpp \
    $$PWD/view/cpp/CPPGlobalEventsDeliver.cpp \
    $$PWD/Game.cpp \
    $$PWD/model/ShotMovementStrategy.cpp
